package main

import (
	"context"
	"errors"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/urfave/cli/v2"
)

func extractRepo(c *cli.Context) error {
	projectPath, ok := os.LookupEnv("CI_PROJECT_PATH")
	if !ok {
		return errors.New("error: $CI_PROJECT_PATH not defined, not in GitLab-CI")
	}

	b, err := BlobFromCtx(c, projectPath)
	if err != nil {
		return err
	}

	if err := downloadBlob(context.Background(), b); err != nil {
		return err
	}
	if err := extractBundle(b); err != nil {
		return err
	}

	return nil
}

func extractBundle(b blob) error {
	targetPath, ok := os.LookupEnv("CI_PROJECT_DIR")
	if !ok {
		return errors.New("error: $CI_PROJECT_DIR not defined, not in GitLab-CI")
	}

	if err := os.MkdirAll(filepath.Dir(targetPath), 0755); err != nil {
		return err
	}

	_ = os.RemoveAll(targetPath)
	cmd := exec.Command("git", "clone", b.bundlePath(), targetPath)

	// we don't need to be verbose in a CI job just started
	// which is why there's no: cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
