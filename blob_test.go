package main

import (
	"os"
	"strings"
	"testing"
)

func TestPathMethods(t *testing.T) {
	b := blob{projectPath: "mep/mep"}

	if !strings.HasPrefix(b.localPath(), os.TempDir()) {
		t.Fatal("foo")
	}

	if !strings.HasSuffix(b.bundlePath(), ".bundle") {
		t.Fatal("bundle has to be the suffix")
	}
}

func TestBucketURL(t *testing.T) {
	b := blob{bucketScheme: "s3", bucketName: "gitlab-runners-bundle-cache", bucketRegion: "eu-west-1"}
	if b.bucketURL() != "s3://gitlab-runners-bundle-cache?region=eu-west-1" {
		t.Fail()
	}
}
