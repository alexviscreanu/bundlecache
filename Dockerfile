FROM golang:1.13 as builder

LABEL maintainer="Zeger-Jan van de Weg <zj@gitlab.com>"

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o bundlecache .

FROM alpine/git:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /app

# Copy the Pre-built binary file from the previous stage
COPY --from=builder /app/bundlecache .

# Command to run the executable
CMD ["./bundlecache"]
