package main

import (
	"context"
	"errors"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/aws/aws-sdk-go/aws/session"
	goblob "gocloud.dev/blob"
	"gocloud.dev/blob/s3blob"
)

func storeBlob(ctx context.Context, b blob) error {
	if err := setupSession(b); err != nil {
		return err
	}

	bucket, err := goblob.OpenBucket(ctx, b.bucketURL())
	if err != nil {
		return err
	}
	defer bucket.Close()

	inBucketPath := strings.TrimPrefix(b.localPath(), os.TempDir())
	w, err := bucket.NewWriter(ctx, inBucketPath, nil)
	if err != nil {
		return err
	}

	f, err := os.Open(b.bundlePath())
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = io.Copy(w, f)
	closeErr := w.Close()

	if err != nil {
		return err
	}

	return closeErr
}

func downloadBlob(ctx context.Context, b blob) error {
	if err := setupSession(b); err != nil {
		return err
	}

	bucket, err := goblob.OpenBucket(ctx, b.bucketURL())
	if err != nil {
		return err
	}
	defer bucket.Close()

	// Open the key "foo.txt" for reading with the default options.
	r, err := bucket.NewReader(ctx, b.objectPath(), nil)
	if err != nil {
		return err
	}
	defer r.Close()

	if err := os.MkdirAll(filepath.Dir(b.bundlePath()), 0755); err != nil {
		return err
	}

	f, err := os.OpenFile(b.bundlePath(), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = io.Copy(f, r)
	return err
}

var errProviderNotSupported = errors.New("unrecognized scheme, only s3:// and gs:// supported")

func setupSession(b blob) error {
	if b.bucketScheme == s3blob.Scheme {
		return setupAWSSession(b)
	}

	// TODO GCS support

	return errProviderNotSupported
}

func setupAWSSession(b blob) error {
	credentials := map[string]string{
		"AWS_ACCESS_KEY_ID":     b.bucketID,
		"AWS_SECRET_ACCESS_KEY": b.bucketSecret,
		"AWS_REGION":            b.bucketRegion,
	}

	for key, value := range credentials {
		if _, set := os.LookupEnv(key); set {
			continue
		}

		if value != "" {
			if err := os.Setenv(key, value); err != nil {
				return err
			}
		}
	}

	_, err := session.NewSession()
	return err
}

func setupGCPSession() error {
	return nil
}
