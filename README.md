## bundlecache

When a pipeline uses high parallelism, the git hosting service might queue the
clone request. For CI, most of the time it's faster to download a `git bundle`
first, than follow up with a `git fetch` and only download a bit of data through
Git. Additionally, this would help save of bandwidth costs if your CI is setup in
another cloud than your self managed GitLab.

The cached bundles need to be created, updated, and used on the CI runner.
`bundlecache` is a small utility to help with the management.

**bundlecache is still experimental**

### Using bundlecache

#### Creating and updating bundles

The advised workflow is to create a scheduled pipeline on GitLab CI, that lists
the projects to cache in the `.gitlab-ci.yml`:


```yaml
:package: cache bundles:
  image: registry.gitlab.com/zj-gitlab/bundlecache:latest
  script: |
    bundlecache create
      gitlab-org/gitaly
      gitlab-org/gitlab
  only:
    - schedules
```


#### Using on GitLab CI

When using the GitLab Runner, a `pre_clone_script` can be configured on the
[runner level][runner-config].

This clone script should download and unbundled to `$CI_BUILDS_DIR`. `bundlecache`
could be used with extract too, but do make sure to always exit with a 0 status
code: `which bundlecache && bundlecache extract || true`.

For authentication either flags or environment variables can be used:

```
$ bundlecache --help
NAME:
   bundlecache - create repository bundle to increase CI speed

USAGE:
   bundlecache [global options] command [command options] [arguments...]

VERSION:
   0.0.0

COMMANDS:
   create   get the repository, create and upload the bundle to object storage
   extract  download and extract the repositoy bundle, must run in GitLab-CI
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --host value                   Set the GitLab host to clone from (default: "https://gitlab.com") [$CI_SERVER_HOST]
   --gitlab-user value            Set the clone user for private projects (default: "gitlab-ci-token")
   --gitlab-token value           Set the access token for GitLab [$CI_JOB_TOKEN]
   --bucket value                 set the bucket with scheme: e.g. s3://gitlab-bundle-cache-bucket [$GCS_BUCKET, $AWS_BUCKET]
   --aws-access-key-id value      set the aws access key [$AWS_ACCESS_KEY_ID]
   --aws-secret-access-key value  set the token used to access the bucket [$AWS_SECRET_ACCESS_KEY]
   --bucket-region value          set the region for the bucket [$AWS_REGION]
   --help, -h                     show help (default: false)
   --version, -v                  print the version (default: false)
```
