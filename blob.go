package main

import (
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"github.com/urfave/cli/v2"
)

type blob struct {
	bucketScheme string
	bucketName   string
	bucketRegion string
	bucketID     string
	bucketSecret string

	projectPath string
}

func BlobFromCtx(c *cli.Context, projectPath string) (blob, error) {
	var b blob

	url, err := url.Parse(c.String("bucket"))
	if err != nil {
		return b, err
	}

	b.bucketScheme = url.Scheme
	b.bucketName = url.Host
	b.bucketID = c.String("aws-access-key-id")
	b.bucketRegion = c.String("bucket-region")
	b.bucketSecret = c.String("aws-secret-access-key")
	b.projectPath = projectPath

	return b, nil
}

func (b *blob) localPath() string {
	return filepath.Join(os.TempDir(), b.projectPath)
}

func (b *blob) bundlePath() string {
	return fmt.Sprintf("%s.bundle", b.localPath())
}

func (b *blob) objectPath() string {
	return strings.TrimPrefix(b.localPath(), os.TempDir())
}

func (b *blob) bucketURL() string {
	return fmt.Sprintf("%s://%s?region=%s", b.bucketScheme, b.bucketName, b.bucketRegion)
}
