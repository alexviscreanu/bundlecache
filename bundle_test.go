package main

import "testing"

func TestValidateHost(t *testing.T) {
	for url, errorExpected := range map[string]bool{
		"foo.com/bar":        true,
		"https://gitlab.com": false,
	} {
		_, err := validateHost(url)
		if (err == nil) == errorExpected {
			t.Fail()
		}

	}
}
