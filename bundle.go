package main

import (
	"context"
	"errors"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/urfave/cli/v2"
)

func bundleRepo(c *cli.Context) error {
	for _, p := range c.Args().Slice() {
		b, err := BlobFromCtx(c, p)
		if err != nil {
			return err
		}

		if err := bundleProject(c, b); err != nil {
			return err
		}

		if err := storeBlob(context.Background(), b); err != nil {
			return err
		}

		fmt.Printf("created a bundle for: %s", p)
	}

	return nil
}

func validateHost(host string) (*url.URL, error) {
	url, err := url.Parse(host)
	if err != nil {
		return nil, err
	}

	if !strings.HasPrefix(url.Scheme, "http") {
		return nil, errors.New("error: host protocol must be HTTP")
	}

	return url, nil
}

func bundleProject(c *cli.Context, b blob) error {
	if err := gitClone(c, b); err != nil {
		return err
	}

	if err := gitBundle(b); err != nil {
		return err
	}

	return nil
}

// gitClone inflates
func gitClone(c *cli.Context, b blob) error {
	// Make sure the sink does not exist
	_ = os.RemoveAll(b.localPath())

	if err := os.MkdirAll(filepath.Dir(b.localPath()), 0755); err != nil {
		return err
	}

	host := c.String("host")
	url, err := validateHost(host)
	if err != nil {
		return err
	}

	user := c.String("user")
	password := c.String("token")

	cloneURL := fmt.Sprintf("%s://%s:%s@%s/%s.git", url.Scheme, user, password, url.Host, b.projectPath)

	cmd := exec.Command("git", "clone", "--bare", cloneURL, b.localPath())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()

}

func gitBundle(b blob) error {
	cmd := exec.Command("git", "-C", b.localPath(), "bundle", "create", b.bundlePath(), "--all")

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
